<?php
define('DB_HOST', 'localhost');
define('DB_NAME', 'name');
define('DB_USER', 'username');
define('DB_PWD', 'password');


function CustomPdo(
    bool $buffered=TRUE,
    bool $persistent=FALSE
    ) : ?PDO
{
    try {
        return new PDO(
            sprintf(
                'mysql:host=%s;dbname=%s;charset=utf8',
                DB_HOST,
                DB_NAME
            ),
            DB_USER,
            DB_PWD,
            array(
                PDO::ATTR_EMULATE_PREPARES => FALSE,
                PDO::ATTR_PERSISTENT => $persistent,
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => $buffered,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            )
        );
    } catch ( PDOException $ex ) {
        error_log(
            sprintf(
                '%s %s %s %s',
                $ex->getMessage(),
                $ex->getCode(),
                $ex->getFile(),
                $ex->getLine()
            )
        );
        die('SQL PDO Error');
    }
}


function MakeQuery(
        PDO $custom_pdo,
        string $query,
        array $params=NULL
    ) : ?array
{
    try {
        $stmt = $custom_pdo->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
    catch (PDOException $ex) {
        error_log(
            sprintf(
                '%s %s %s %s',
                $ex->getMessage(),
                $ex->getCode(),
                $ex->getFile(),
                $ex->getLine()
            )
        );
        die('SQL PDOStmt Error.');
    }
}


function MakeOneRowQuery(
        PDO $custom_pdo,
        string $query,
        array $params=NULL
    ) : ?array
{
    try {
        $stmt = $custom_pdo->prepare($query);
        $stmt->execute($params);
        return $stmt->fetch();
    }
    catch (PDOException $ex) {
        error_log(
            sprintf(
                '%s %s %s %s',
                $ex->getMessage(),
                $ex->getCode(),
                $ex->getFile(),
                $ex->getLine()
            )
        );
        die('SQL PDOStmt Error.');
    }
}


function SafeMakeQuery(
        PDO $custom_pdo,
        string $query,
        array $params
    ) : ?bool
{
    try {
        $custom_pdo->beginTransaction();
        $stmt = $custom_pdo->prepare($query);
        $stmt->execute($params);
        $custom_pdo->commit();
        return true;
    }
    catch (PDOException $ex) {
        error_log(
            sprintf(
                '%s %s %s %s',
                $ex->getMessage(),
                $ex->getCode(),
                $ex->getFile(),
                $ex->getLine()
            )
        );
        $custom_pdo->rollback();
        die('SQL PDOStmt Error.');
    }
}


function ExampleFunction(
    int $sku,
    int $orderid
) : ?array
{
    return makeQuery(
        customPdo(),
        "SELECT qty FROM orderProducts 
        WHERE sku = :sku AND orderid = :orderid ",
        array(
            ':sku' => $sku,
            ':orderid' => $orderid
        )
    );
}